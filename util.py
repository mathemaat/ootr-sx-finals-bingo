from enum import Enum


# see https://docs.python.org/3/library/enum.html#using-a-descriptive-string
class DescriptiveEnum(Enum):

    def __repr__(self):
        return '<%s.%s>' % (self.__class__.__name__, self.name)
